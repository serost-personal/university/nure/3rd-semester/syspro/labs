#include <windows.h>
#include <fstream>

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow) {
    const wchar_t CLASS_NAME[] = L"Sample Window Class";

    WNDCLASS wc = { 0 };

    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    HWND hwnd = CreateWindowEx(
        0,
        CLASS_NAME,
        L"WinAPI Menu Example",
        WS_OVERLAPPEDWINDOW,

        CW_USEDEFAULT, CW_USEDEFAULT, 500, 300,

        NULL,
        NULL,
        hInstance,
        NULL
    );

    if (hwnd == NULL) {
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);

    MSG msg = { 0 };
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

void OnCreateFile(HWND hwnd) {
    OPENFILENAME ofn;
    wchar_t fileName[MAX_PATH];

    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hwnd;
    ofn.lpstrFile = fileName;
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = MAX_PATH;
    ofn.lpstrFilter = L"All Files\0*.*\0";
    ofn.nFilterIndex = 1;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    if (GetSaveFileName(&ofn)) {
        // Check if the file already exists
        if (GetFileAttributes(fileName) != INVALID_FILE_ATTRIBUTES) {
            // File exists, ask for confirmation to overwrite
            if (MessageBox(hwnd, L"Do you want to overwrite the existing file?", L"File Exists", MB_YESNO | MB_ICONWARNING) == IDNO) {
                return; // User chose not to overwrite, exit function
            }
        }

        // Perform file creation logic here
        HANDLE hFile = CreateFile(fileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        if (hFile != INVALID_HANDLE_VALUE) {
            // File created successfully, write "Hello, World!" to the file
            const wchar_t* content = L"Hello, World!";
            DWORD bytesWritten;
            WriteFile(hFile, content, lstrlen(content) * sizeof(wchar_t), &bytesWritten, NULL);

            // Close the file handle
            CloseHandle(hFile);

            MessageBox(hwnd, L"File created successfully!", L"Success", MB_OK | MB_ICONINFORMATION);
        }
        else {
            // Error creating file
            MessageBox(hwnd, L"Error creating file!", L"Error", MB_OK | MB_ICONERROR);
        }
    }
}

void OnDeleteFile(HWND hwnd) {
    OPENFILENAME ofn;
    wchar_t fileName[MAX_PATH];

    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hwnd;
    ofn.lpstrFile = fileName;
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = MAX_PATH;
    ofn.lpstrFilter = L"All Files\0*.*\0";
    ofn.nFilterIndex = 1;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    if (GetOpenFileName(&ofn)) {
        if (DeleteFile(fileName)) {
            MessageBox(hwnd, L"File successfully deleted!", L"Success", MB_OK | MB_ICONINFORMATION);
        }
        else {
            MessageBox(hwnd, L"File not found or unable to delete!", L"Error", MB_OK | MB_ICONERROR);
        }
    }
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_CREATE: {
        HMENU hMenu, hSubMenu;

        hMenu = CreateMenu();
        hSubMenu = CreatePopupMenu();
        AppendMenu(hSubMenu, MF_STRING, 1, L"Create File");
        AppendMenu(hSubMenu, MF_STRING, 2, L"Delete File");
        AppendMenu(hMenu, MF_POPUP, (UINT_PTR)hSubMenu, L"File");

        SetMenu(hwnd, hMenu);
    } break;

    case WM_COMMAND: {
        switch (LOWORD(wParam)) {
        case 1:
            OnCreateFile(hwnd);
            break;
        case 2:
            OnDeleteFile(hwnd);
            break;
        }
    } break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}
