#include <iostream>
#include <string>

int stringLength(const char* str) {
    int l;
    __asm {
        mov ecx, str  // Load the address of the string into ecx
        xor eax, eax  // Clear eax to store the length

        countLoop :
        cmp byte ptr[ecx], 0  // Compare the current character with null terminator
            je endLoop             // If null terminator is found, jump to endLoop
            inc ecx                // Move to the next character in the string
            inc eax                // Increment the length counter
            jmp countLoop          // Jump back to countLoop

            endLoop :
        mov l, eax  // Store the length in the variable 'l'
    }
    return l;
}

void stringCopy(char* dest, const char* src) {
    __asm {
        mov esi, src  // Load the source address into esi
        mov edi, dest // Load the destination address into edi

        copyLoop :
        mov al, [esi]  // Load the byte from the source into al
            mov[edi], al  // Store the byte to the destination
            inc esi        // Move to the next character in the source
            inc edi        // Move to the next character in the destination
            cmp al, 0      // Compare the copied character with null terminator
            jne copyLoop   // If not null terminator, continue copying

    }
}

void stringConcatenate(char* dest, const char* src) {
    __asm {
        mov esi, src    // Load the source address into esi
        mov edi, dest   // Load the destination address into edi

        // Find the end of the destination string
        mov ecx, 0
        mov eax, edi
        findEndLoop :
        cmp byte ptr[eax], 0  // Compare the current character with null terminator
            je endFindEndLoop      // If null terminator is found, jump to endFindEndLoop
            inc eax                // Move to the next character in the destination
            inc ecx                // Increment the length counter
            jmp findEndLoop        // Jump back to findEndLoop

            endFindEndLoop :

        // Concatenate the source string to the destination
    concatLoop:
        mov al, [esi]          // Load the byte from the source into al
            mov[edi + ecx], al    // Store the byte to the destination at the end
            inc esi                // Move to the next character in the source
            inc ecx                // Increment the length counter
            cmp al, 0              // Compare the copied character with null terminator
            jne concatLoop         // If not null terminator, continue concatenating
    }
}

int stringCompare(const char* str1, const char* str2) {
    int result;
    __asm {
        mov esi, str1   // Load the address of str1 into esi
        mov edi, str2   // Load the address of str2 into edi

        compareLoop :
        mov al, [esi]          // Load byte from str1 into al
            mov bl, [edi]          // Load byte from str2 into bl
            cmp al, bl             // Compare bytes
            jne endCompare         // If bytes are not equal, jump to endCompare
            cmp al, 0              // Check for null terminator
            je equalStrings        // If null terminator found, strings are equal
            inc esi                // Move to next character in str1
            inc edi                // Move to next character in str2
            jmp compareLoop        // Jump back to compareLoop if characters are equal

            equalStrings :
        mov result, 0          // Set result to 0 (equal strings)
            jmp endFunction        // Jump to end of function

            endCompare :
        movzx eax, al          // Zero-extend al to eax
            movzx ebx, bl          // Zero-extend bl to ebx
            sub eax, ebx           // Calculate difference between characters
            mov result, eax        // Set result to difference
            jmp endFunction        // Jump to end of function

            endFunction :
    }
    return result;
}


int main() {
    const char* str1 = "2ello";
    const char* str2 = "1orld";

    // Test the functions
    std::cout << "String 1: " << str1 << std::endl;
    std::cout << "String 2: " << str2 << std::endl;

    // Size of strings
    std::cout << "Size of String 1: " << stringLength(str1) << std::endl;
    std::cout << "Size of String 2: " << stringLength(str2) << std::endl;

    // Copy strings
    char copiedString[20];
    stringCopy(copiedString, str1);
    std::cout << "Copied String: " << copiedString << std::endl;

    // Concatenate strings
    char concatenatedString[20];
    memset(concatenatedString, '\0', 20);
    stringConcatenate(concatenatedString, str1);
    stringConcatenate(concatenatedString, str2);
    std::cout << "Concatenated String: " << concatenatedString << std::endl;

    // Compare strings
    int comparisonResult = stringCompare(str1, str2);
    if (comparisonResult < 0) {
        std::cout << "String 1 is less than String 2" << std::endl;
    } else if (comparisonResult > 0) {
        std::cout << "String 1 is greater than String 2" << std::endl;
    } else {
        std::cout << "String 1 is equal to String 2" << std::endl;
    }

    return 0;
}
