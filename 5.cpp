#include <windows.h>
#include <iostream>

struct TimeData {
    int hours;
    int minutes;
    int seconds;
    CRITICAL_SECTION cs;
    HANDLE semaphore;
};

TimeData g_timeData;
HWND g_hWnd;

DWORD WINAPI ReadTimeThread(LPVOID lpParam) {
    while (true) {
        SYSTEMTIME sysTime;
        GetLocalTime(&sysTime);

        EnterCriticalSection(&g_timeData.cs);

        g_timeData.hours = sysTime.wHour;
        g_timeData.minutes = sysTime.wMinute;
        g_timeData.seconds = sysTime.wSecond;

        LeaveCriticalSection(&g_timeData.cs);

        ReleaseSemaphore(g_timeData.semaphore, 1, NULL);

        Sleep(1000);
    }

    return 0;
}

DWORD WINAPI DisplayTimeThread(LPVOID lpParam) {
    while (true) {
        WaitForSingleObject(g_timeData.semaphore, INFINITE);

        EnterCriticalSection(&g_timeData.cs);

        char timeString[9];
        sprintf_s(timeString, "%02d:%02d:%02d", g_timeData.hours, g_timeData.minutes, g_timeData.seconds);

        SetWindowTextA(g_hWnd, timeString);

        LeaveCriticalSection(&g_timeData.cs);

        Sleep(1000);
    }

    return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch (message) {
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

int main() {
    InitializeCriticalSection(&g_timeData.cs);
    g_timeData.semaphore = CreateSemaphore(NULL, 0, 1, NULL);

    HINSTANCE hInstance = GetModuleHandle(NULL);

    WNDCLASS wc = { 0 };
    wc.lpfnWndProc = WndProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = L"TimeDisplayWindowClass";

    RegisterClass(&wc);

    g_hWnd = CreateWindow(
        L"TimeDisplayWindowClass", L"Time Display",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, 300, 100,
        NULL, NULL, hInstance, NULL
    );

    ShowWindow(g_hWnd, SW_SHOWNORMAL);
    UpdateWindow(g_hWnd);

    HANDLE hReadThread = CreateThread(NULL, 0, ReadTimeThread, NULL, 0, NULL);
    HANDLE hDisplayThread = CreateThread(NULL, 0, DisplayTimeThread, NULL, 0, NULL);

    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    WaitForSingleObject(hReadThread, INFINITE);
    WaitForSingleObject(hDisplayThread, INFINITE);

    DeleteCriticalSection(&g_timeData.cs);
    CloseHandle(g_timeData.semaphore);
    CloseHandle(hReadThread);
    CloseHandle(hDisplayThread);

    return 0;
}
