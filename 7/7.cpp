#undef UNICODE
#include <windows.h>
#include <iostream>
#include <string>

#include "rect.h"

// Function pointer types for dynamically loaded functions
typedef int (*FibonacciFunction)(int);
typedef int (*FactorialFunction)(int);

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    // Register the window class
    const char CLASS_NAME[] = "Sample Window Class";

    WNDCLASS wc = {};
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    // Create the window
    HWND hwnd = CreateWindowEx(
        0,                              // Optional window styles
        CLASS_NAME,                     // Window class
        "Dynamic DLL Loading",          // Window text
        WS_OVERLAPPEDWINDOW,            // Window style
        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, 500, 300,
        NULL,       // Parent window    
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
    );

    if (hwnd == NULL) {
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);

    // Run the message loop
    MSG msg = {};
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

// Handle window messages
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_LBUTTONDOWN: {
        // Load the calculation DLL
        HMODULE calcDLL = LoadLibrary("calc.dll");
        if (calcDLL == NULL) {
            MessageBox(hwnd, "Failed to load calc.dll", "Error", MB_OK | MB_ICONERROR);
            break;
        }

        // Get function pointers
        FibonacciFunction fibonacci = (FibonacciFunction)GetProcAddress(calcDLL, "fibonacci");
        FactorialFunction factorial = (FactorialFunction)GetProcAddress(calcDLL, "factorial");

        if (fibonacci != NULL && factorial != NULL) {
            // Call the functions
            int fibResult = fibonacci(6);
            int factResult = factorial(5);

            // Display results in message boxes
            std::string fibMsg = "Fibonacci(6): " + std::to_string(fibResult);
            std::string factMsg = "Factorial(5): " + std::to_string(factResult);
            MessageBox(hwnd, fibMsg.c_str(), "Result", MB_OK);
            MessageBox(hwnd, factMsg.c_str(), "Result", MB_OK);

            // Call drawRectangle function
            drawRectangle(30, 30, 100, 50);
        }
        else {
            MessageBox(hwnd, "Failed to load functions from calc.dll", "Error", MB_OK | MB_ICONERROR);
        }

        FreeLibrary(calcDLL);  // Release the DLL
        break;
        break;
    }

    case WM_DESTROY: {
        PostQuitMessage(0);
        break;
    }

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
    return 0;
}
