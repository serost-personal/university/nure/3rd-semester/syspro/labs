#include "rect.h"
#include "pch.h"

extern "C" __declspec(dllexport) void drawRectangle(int x, int y, int width, int height) {
    HDC hdc = GetDC(GetForegroundWindow());
    Rectangle(hdc, x - width / 2, y - height / 2, x + width / 2, y + height / 2);
    ReleaseDC(GetForegroundWindow(), hdc);
}