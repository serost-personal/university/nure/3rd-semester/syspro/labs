#undef UNICODE
#include <windows.h>
#include <string>
#define WINDOW_CLASS_NAME "WinAPIButtonsClass"

#define BUTTON_ID_BASE 1000

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    WNDCLASS wc = {};
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = WINDOW_CLASS_NAME;
    RegisterClass(&wc);

    HWND hwnd = CreateWindowEx(0, WINDOW_CLASS_NAME, "WinAPI Buttons", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 400, 600, NULL, NULL, hInstance, NULL);

    for (int i = 0; i < 10; ++i) {
        CreateWindow("BUTTON", ("Button " + std::to_string(i + 1)).c_str(), WS_VISIBLE | WS_CHILD, 50, 30 + 30 * i, 100, 25, hwnd, (HMENU)(BUTTON_ID_BASE + i), hInstance, NULL);
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    MSG msg = {};
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_COMMAND:
        if (HIWORD(wParam) == BN_CLICKED) {
            int buttonID = LOWORD(wParam);
            int index = buttonID - BUTTON_ID_BASE;
            if (index >= 0 && index < 10) {
                std::string fileName = "laba" + std::to_string(index + 1) + ".exe";
                ShellExecute(NULL, "open", fileName.c_str(), NULL, NULL, SW_SHOWNORMAL);
            }
        }
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
    return 0;
}
