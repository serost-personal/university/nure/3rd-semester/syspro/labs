#include <iostream>
#include <bitset>
#include <windows.h>

void countBits(DWORD value, int* nzeroes, int* nones) {
    int count0 = 0;
    int count1 = 0;
    __asm {
        mov eax, value      // Place the value in eax
        mov ecx, 32         // Number of bits in a 32-bit number
        xor ebx, ebx        // Clear the counter for 0
        xor edx, edx        // Clear the counter for 1
        loopStart:
        test eax, 1             // Test the least significant bit
            jz isZero           // If it's 0, jump to isZero
            inc edx             // Increment the counter for 1
            jmp nextBit         // Jump to nextBit
        isZero:
            inc ebx             // Increment the counter for 0
        nextBit:
            shr eax, 1          // Shift the value right by 1 bit
            loop loopStart      // Repeat the loop 32 times
            mov count0, ebx     // Return the count of 0
            mov count1, edx     // Return the count of 1
    }
    *nzeroes = count0;
    *nones = count1;
}

int main() {
    DWORD randomValue1 = rand();
    DWORD randomValue2 = rand();
    int rv1_zeroes, rv1_ones, rv2_zeroes, rv2_ones;
    countBits(randomValue1, &rv1_zeroes, &rv1_ones);
    countBits(randomValue2, &rv2_zeroes, &rv2_ones);

    std::cout << "Random Value 1: " << randomValue1 << " (Binary: " << std::bitset<32>(randomValue1) << ")" << std::endl;
    std::cout << "Number of 0 bits: " << rv1_zeroes << std::endl;
    std::cout << "Number of 1 bits: " << rv1_ones << std::endl;

    std::cout << "Random Value 2: " << randomValue2 << " (Binary: " << std::bitset<32>(randomValue2) << ")" << std::endl;
    std::cout << "Number of 0 bits: " << rv2_zeroes << std::endl;
    std::cout << "Number of 1 bits: " << rv2_ones << std::endl;
    return 0;
}
